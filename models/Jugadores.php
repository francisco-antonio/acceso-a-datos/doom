<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $cod_jugador
 * @property int|null $cod_personaje
 * @property string|null $nombre
 *
 * @property Personajes $codJugador
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_jugador'], 'required'],
            [['cod_jugador', 'cod_personaje'], 'integer'],
            [['nombre'], 'string', 'max' => 10],
            [['cod_jugador'], 'unique'],
            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_jugador' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_jugador' => 'Cod Jugador',
            'cod_personaje' => 'Cod Personaje',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_jugador']);
    }
}
