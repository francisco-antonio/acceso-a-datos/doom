<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "razas".
 *
 * @property string $nombre_raza
 *
 * @property Demonios[] $demonios
 * @property Demonios[] $demonios0
 * @property Habilidades[] $habilidades
 */
class Razas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'razas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_raza'], 'required'],
            [['nombre_raza'], 'string', 'max' => 10],
            [['nombre_raza'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre_raza' => 'Nombre Raza',
        ];
    }

    /**
     * Gets query for [[Demonios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDemonios()
    {
        return $this->hasMany(Demonios::className(), ['nombre_razas' => 'nombre_raza']);
    }

    /**
     * Gets query for [[Demonios0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDemonios0()
    {
        return $this->hasMany(Demonios::className(), ['nombre_razas' => 'nombre_raza']);
    }

    /**
     * Gets query for [[Habilidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHabilidades()
    {
        return $this->hasMany(Habilidades::className(), ['nombre_raza' => 'nombre_raza']);
    }
}
