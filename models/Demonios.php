<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "demonios".
 *
 * @property int $cod_demonio
 * @property int|null $cod_personaje
 * @property int|null $numero
 * @property string|null $nombre_razas
 * @property string|null $nombre_mapas
 * @property string|null $nombre_demonio
 *
 * @property Razas $nombreRazas
 * @property Personajes $codPersonaje
 * @property Mapas $nombreMapas
 * @property Razas $nombreRazas0
 */
class Demonios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'demonios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_demonio'], 'required'],
            [['cod_demonio', 'cod_personaje', 'numero'], 'integer'],
            [['nombre_razas'], 'string', 'max' => 10],
            [['nombre_mapas', 'nombre_demonio'], 'string', 'max' => 12],
            [['cod_demonio'], 'unique'],
            [['nombre_razas'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['nombre_razas' => 'nombre_raza']],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
            [['nombre_mapas'], 'exist', 'skipOnError' => true, 'targetClass' => Mapas::className(), 'targetAttribute' => ['nombre_mapas' => 'nombre_mapa']],
            [['nombre_razas'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['nombre_razas' => 'nombre_raza']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_demonio' => 'Cod Demonio',
            'cod_personaje' => 'Cod Personaje',
            'numero' => 'Numero',
            'nombre_razas' => 'Nombre Razas',
            'nombre_mapas' => 'Nombre Mapas',
            'nombre_demonio' => 'Nombre Demonio',
        ];
    }

    /**
     * Gets query for [[NombreRazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreRazas()
    {
        return $this->hasOne(Razas::className(), ['nombre_raza' => 'nombre_razas']);
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[NombreMapas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreMapas()
    {
        return $this->hasOne(Mapas::className(), ['nombre_mapa' => 'nombre_mapas']);
    }

    /**
     * Gets query for [[NombreRazas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreRazas0()
    {
        return $this->hasOne(Razas::className(), ['nombre_raza' => 'nombre_razas']);
    }
}
