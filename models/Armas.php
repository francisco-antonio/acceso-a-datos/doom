<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "armas".
 *
 * @property int $numero
 * @property int|null $cod_personaje
 * @property string|null $nombre
 * @property string|null $municion
 *
 * @property Personajes $codPersonaje
 */
class Armas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'armas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero'], 'required'],
            [['numero', 'cod_personaje'], 'integer'],
            [['nombre'], 'string', 'max' => 10],
            [['municion'], 'string', 'max' => 12],
            [['numero'], 'unique'],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numero' => 'Numero',
            'cod_personaje' => 'Cod Personaje',
            'nombre' => 'Nombre',
            'municion' => 'Municion',
        ];
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }
}
