<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mapas".
 *
 * @property string $nombre_mapa
 * @property string|null $puntos_control
 * @property string|null $foto
 *
 * @property Demonios[] $demonios
 * @property Zonas[] $zonas
 */
class Mapas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mapas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_mapa'], 'required'],
            [['nombre_mapa'], 'string', 'max' => 12],
            [['puntos_control'], 'string', 'max' => 20],
            [['foto'], 'string', 'max' => 25],
            [['nombre_mapa'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre_mapa' => 'Nombre Mapa',
            'puntos_control' => 'Puntos Control',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Demonios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDemonios()
    {
        return $this->hasMany(Demonios::className(), ['nombre_mapas' => 'nombre_mapa']);
    }

    /**
     * Gets query for [[Zonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasMany(Zonas::className(), ['nombre_mapa' => 'nombre_mapa']);
    }
}
