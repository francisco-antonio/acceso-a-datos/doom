﻿
DROP DATABASE IF EXISTS doom;
CREATE DATABASE doom;

USE doom;
/* crear las tablas */

-- Jugadores

  CREATE OR REPLACE TABLE Jugadores(
cod_jugador int (1),
cod_personaje int (1) ,
nombre varchar(10),
PRIMARY KEY(cod_jugador)

  );

-- Personajes

  CREATE OR REPLACE TABLE Personajes(
cod_personaje int (1),
nombre varchar(10),
descripcion varchar(110),
es_secundario boolean,
PRIMARY KEY(cod_personaje)

  );

-- Armas

  CREATE OR REPLACE TABLE Armas(
numero int (2),
cod_personaje int (1),
nombre varchar(10),
municion varchar(12),
PRIMARY KEY(numero)

  );

-- Demonios

   CREATE OR REPLACE TABLE Demonios(
cod_demonio int (1),
cod_personaje int (1),
numero int (2),
nombre_razas varchar(10),
nombre_mapas varchar(12),
nombre_demonio varchar(12),
PRIMARY KEY (cod_demonio)

  );

-- Razas

  CREATE OR REPLACE TABLE Razas(
nombre_raza varchar(10),
PRIMARY KEY(nombre_raza)

  );

-- Habilidades

  CREATE OR REPLACE TABLE Habilidades(
 id int (1),
 nombre_raza varchar(10),
 habilidades varchar(10),
 PRIMARY KEY(id)

    );

  -- Mapas

    CREATE OR REPLACE TABLE Mapas(
  nombre_mapa varchar(12),
  puntos_control varchar(20),
  foto varchar(25),
  PRIMARY KEY (nombre_mapa)

);

  -- Zonas

    CREATE OR REPLACE TABLE Zonas(
   cod_zona int (1),
   es_secreta boolean,
   nombre_mapa varchar (20),
  PRIMARY KEY (cod_zona)
  );


/** creacion de las restricciones **/

  -- Armas

ALTER TABLE Armas
  ADD CONSTRAINT fk_armas_personaje
  FOREIGN KEY (cod_personaje)
  REFERENCES Personajes(cod_personaje);

-- Jugadores

ALTER TABLE Jugadores
  ADD CONSTRAINT fk_jugador_personaje
  FOREIGN KEY (cod_jugador)
  REFERENCES Personajes (cod_personaje),
  ADD CONSTRAINT uk_personaje_jugador
  UNIQUE KEY (cod_jugador);

--  Demonios

  ALTER TABLE Demonios
  ADD CONSTRAINT fk_demonios_personaje
  FOREIGN KEY (cod_personaje)
  REFERENCES Personajes(cod_personaje);


-- Habilidades

  ALTER TABLE Habilidades
  ADD CONSTRAINT fk_habilidades_razas
  FOREIGN KEY (nombre_raza)
  REFERENCES Razas(nombre_raza);

  -- Zonas

    ALTER TABLE Zonas
  ADD CONSTRAINT fk_zonas_mapas
  FOREIGN KEY (nombre_mapa)
  REFERENCES Mapas(nombre_mapa);

-- Demonios
  ALTER TABLE Demonios
  ADD CONSTRAINT fk_demonios_mapas
  FOREIGN KEY (nombre_mapas)
  REFERENCES Mapas(nombre_mapa);

  -- Razas
    ALTER TABLE Demonios
  ADD CONSTRAINT fk_demonios_razas
  FOREIGN KEY (nombre_razas)
  REFERENCES Razas(nombre_raza);

   