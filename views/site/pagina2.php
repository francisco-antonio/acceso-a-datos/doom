<!DOCTYPE html>
<html lang="es">

<head>
	<title>DOOMWIKIAPP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Fashion Trendz Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>

	<!--  archivos  css -->
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
	<link href="css/style.css" rel='stylesheet' type='text/css' /><!-- personalizado css -->
	<link href="css/owl-carousel.css" rel='stylesheet' type='text/css' /><!-- owl carousel css - main slider -->
	<link href="css/font-awesome.min.css" rel="stylesheet"><!-- fontawesome css -->
	<!-- //archivos  css -->

	<!-- fonts de google -->
	<link href="//fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	<!-- //fonts de google  -->

</head>

<body>

	<!-- header -->
	<header>
		<div class="container">
			<div class="header d-lg-flex">
				<div class="header-agile d-flex">
					<h1 class="mr-lg-0 mr-4">
                                            <a class="navbar-brand logo" href="index.php">
                                                    <img style="align-items:center" alt="Logotipo Doom Wiki" src="images\logo app.png" width="80" height="70"  />
						</a>
					</h1>
					<div class="social social-invisible mt-3">
						<ul>
							<li class="mr-sm-3 mr-2 icons"><a href="#"><span class="fa fa-facebook" aria-hidden="true"></span></a>
							</li>
							
							
							<li class="mr-sm-3 mr-2 icons"><a href="#"><span class="fa fa-twitter" aria-hidden="true"></span></a></li>
							<li class="icons"><a href="#"><span class="fa fa-pinterest" aria-hidden="true"></span></a></li>
						</ul>
					</div>
				</div>
				<div class="nav_w3ls mt-3">
					<nav>
						<label for="drop" class="toggle mt-lg-0 mt-1"><span class="fa fa-bars" aria-hidden="true"></span></label>
						<input type="checkbox" id="drop" />
						<ul class="menu">
                                                    <li class="mr-lg-3 mr-2 active"><a href="index.php">Inicio</a></li>
							<li class="mr-lg-3 mr-2"><a href="#about">Sobrenosotros </a></li>
							<li class="mr-lg-3 mr-2 p-0">
                                                            
								<!-- Desplegable 1 linea 65 -->
                                                                
								<label for="drop-2" class="toggle">Secciones <span class="fa fa-angle-down" aria-hidden="true"></span>
								</label>
								<a href="#">Secciones <span class="fa fa-angle-down" aria-hidden="true"></span></a>
								<input type="checkbox" id="drop-2" />
								<ul class="inner-dropdown">
									<li><a href="#services">Servicios</a></li>
									<li><a href="#facts">factores compañia</a></li>
									<li><a href="#subscribe">suscripcion</a></li>
								</ul>
                                                                
                                                    
							</li>
							<li class="mr-lg-3 mr-2"><a href="#gallery">Galeria</a></li>
							<li class="mr-lg-3 mr-2"><a href="#contact">contactanos</a></li>
                                                        
                                                        <li class="mr-lg-3 mr-2 p-0">
                                                                    <!-- Desplegable 2 linea 75 -->
                                                                
                                                                <label for="drop-3" class="toggle">Apartados <span class="fa fa-angle-down" aria-hidden="true"></span>
								</label>
								<a href="#">Apartados <span class="fa fa-angle-down" aria-hidden="true"></span></a>
								<input type="checkbox" id="drop-3" />
								<ul class="inner-dropdown">
									<li><a href="#services">Jugadores</a></li>
									<li><a href="#facts">Personajes</a></li>
									<li><a href="#subscribe">Armas</a></li>
                                                                        <li><a href="#subscribe">Demonios</a></li>
                                                                        <li><a href="#subscribe">Razas</a></li>
                                                                        <li><a href="#subscribe">Mapas</a></li>
								</ul>
                                                                
                                                                <!-- Desplegable 2 final -->
                                                        
							</li>
						</ul>
					</nav>
				</div>
				<div class="social mt-3 ml-auto">
					<ul>
						<li class="mr-3 icons"><a href="#"><span class="fa fa-facebook" aria-hidden="true"></span></a></li>
						<li class="mr-3 icons"><a href="#"><span class="fa fa-instagram" aria-hidden="true"></span></a></li>
						<li class="mr-3 icons"><a href="#"><span class="fa fa-twitter" aria-hidden="true"></span></a></li>
						<li class=" icons"><a href="#"><span class="fa fa-pinterest" aria-hidden="true"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	<!-- //header -->


	<!-- MAIN SLIDER BORRADO -->
    <div style="background-color:green;">
    
    </div>
	<head>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
        <!-- Resumen -->
   <div class="text-centered div1"> 
    <p style="color: black"> Esta aplicación web esta dedicada a explicar y informar todo lo posible sobre el juego DOOM, el cual tiene una historia interesante aunque no se le da mucha importancia.</p>
<img  alt="Logotipo Doom Wiki" src="images/logo app.png" width="199" height="150"  />
        </div>
        <br>
        <br>
    </head>
	<!-- /MAIN SLIDER BORRADO -->

	<!-- SOBRE EL DOOM -->
	<section class="about py-5" id="about">
		<div class="container py-lg-5 py-md-3">
			<div class="row">
				<div class="col-lg-6 about-padding">
                    
					<h4 style="color: whitesmoke" class="mt-lg-5">¿QUE ES EL DOOM?</h4>
                    
					<p style="color: whitesmoke" class="mt-3">Doom (oficialmente escrito DOOM, y ocasionalmente DooM por los fans, basado en el estilo de las letras del logo de Doom)  es un videojuego FPS (First Person Shooter o disparos en primera persona) creado por Id Software en 1993. El Doom original funcionaba bajo el sistema operativo DOS.

El juego consiste en personificar a un marine espacial que se encuentra de rutina en una estación en Phobos , una de las lunas de Marte . En un segundo, las puertas del Infierno quedan abiertas, dejando libres a un sinnúmero de demonios, espíritus inmundos, zombis, que infestan la base en cuestión de horas. Eres el único ser humano superviviente en la estación y tu misión es ir logrando pasar con vida de nivel en nivel (como en el Wolfenstein 3D).</p>
                    
				</div>
                
				<div class="col-lg-6 px-sm-0 img-div">
					<img src="images/ab1.jpg" alt="" class="img-fluid" />
					<!--<img src="images/ab1-alt.jpg" alt="" class="img-fluid position-img" /> -->
				</div>
                
				<div class="col-lg-6 px-sm-0 img-div mt-lg-0 mt-md-4 mt-0">
					<img src="images/ab2.jpg" alt="" class="img-fluid" />
					<img src="images/ab2-alt.jpg" alt="" class="img-fluid position-img1" />
				</div>
                
				<div class="col-lg-6 about-padding">
                    
					<h4 style="color: whitesmoke" class="mt-lg-5">Historia Original</h4>
                    
					<p style="color: whitesmoke" class="mt-3">Eres un marine, de los más fuertes y entrenados de la Tierra, experimentado en combate y listo para la acción. Hace 3 años golpeaste a un oficial superior por ordenar a sus soldados que dispararan contra un grupo de manifestantes civiles. Considerándote como peligroso, decidieron trasladarte a la base en Marte, sector espacial de la UAC (Union Aerospace Corporation). La UAC es un conglomerado multi-planetario con instalaciones de investigación y desechos radioactivos en el planeta Marte y sus dos lunas, Phobos y Deimos. Tus primeros días los pasaste sentado en la sala de vigilancia, viendo videos restringidos y material ultrasecreto como si fuera TV por cable, no habiendo mucha acción allí. Ya habiendo pasado algún tiempo, repentinamente Marte recibió un mensaje desde Fobos "¡Requerimos de soporte militar inmediato! ¡Algo terrible y monstruoso está saliendo por los portales! ¡Los sistemas no responden! Los sistemas computarizados se han vuelto locos!" El resto era simplemente incoherente.</p>
                    
				</div>
			</div>
		</div>
	</section>
	<!-- //SOBRE EL DOOM -->

	<!-- servicios -->
	<section class="services py-5" id="services">
		<div class="container py-xl-5 py-lg-3">
			<div class="row py-xl-3">
				<div class="col-lg-4 mt-lg-5">
					<p class="title">Servicios</p>
					<h3 class="heading mb-4">Somos lo mejor en</h3>
					<p>Sed ut perspiciatis unde omnis istem natus error sit voluptatem accusa ntium doloret emque
						laudantium, totam rem aperiam, et eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae
						vitae
						sed. </p>
				</div>
				<div class="col-lg-8 mt-lg-0 mt-5">
					<div class="row">
						<div class="col-md-6">
							<div class="bottom-gd-ser p-4">
								<div class="row">
									<div class="col-sm-2 bottom-gd-icon">
                                                                            <!-- icono shop cambiar -->
									<img style="align-items:center" alt="Logotipo Doom Wiki" src="images\logo app.png" width="80" height="70"  />	
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4">
										<h4 class="mb-sm-3 mb-2">Fashion Modelling</h4>
										<p>Integer sit amet mattis quam, sit amet ultricies velit.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 my-md-0 my-4">
							<div class="bottom-gd-ser p-4">
								<div class="row">
									<div class="col-sm-2 bottom-gd-icon">
                                                                            
                                                                            <!-- cambiar barita magica -->
                                                                            
										<span class="fa fa-magic" aria-hidden="true"></span>
                                                                                
                                                                                <!-- cambiar barita magica -->
                                                                                
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4">
										<h4 class="mb-sm-3 mb-2">Stylish Makeup</h4>
										<p>Integer sit amet mattis quam, sit amet ultricies velit.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-md-5">
						<div class="col-md-6">
							<div class="bottom-gd-ser p-4">
								<div class="row">
									<div class="col-sm-2 bottom-gd-icon">
                                                                            
                                                                            <!-- cambiar copo de nieve -->
                                                                            
										<span class="fa fa-snowflake-o" aria-hidden="true"></span>
                                                                                
                                                                                 <!-- cambiar copo de nieve -->
                                                                                 
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4">
										<h4 class="mb-sm-3 mb-2">Fashion Shows </h4>
										<p>Integer sit amet mattis quam, sit amet ultricies velit.</p>
									</div>
								</div>
							</div>
                                                    
						</div>
                                            
                                            
						<div class="col-md-6 mt-md-0 mt-4">
							<div class="bottom-gd-ser p-4">
								<div class="row">
									<div class="col-sm-2 bottom-gd-icon">
                                                                            
                                                                            <!-- cambiar icono del ojo -->
                                                                            
										<span class="fa fa-weibo" aria-hidden="true"></span>
                                                                                
                                                                                 <!-- cambiar icono del ojo -->
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4">
										<h4 class="mb-sm-3 mb-2">Ramp Walks</h4>
										<p>Integer sit amet mattis quam, sit amet ultricies velit.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //servicio -->

	<!-- Galeria -->
	<section class="gallery py-5" id="gallery">
		<div class="container py-lg-5 py-3">
			<p class="text-center">Our Fashion Models</p>
			<h3 class="heading mb-4 text-center">Fashion Gallery</h3>
			<div class="row news-grids text-center">
				<div class="col-md-4 col-sm-6 gal-img">
					<a href="#gal1"><img src="images/1.jpg" alt="news image" class="img-fluid"></a>
					<a href="#gal2"><img src="images/2.jpg" alt="news image" class="img-fluid"></a>

				</div>

				<div class="col-md-4 col-sm-6 gal-img">
					<a href="#gal3"><img src="images/3.jpg" alt="news image" class="img-fluid"></a>

					<a href="#gal4"><img src="images/4.jpg" alt="news image" class="img-fluid"></a>
				</div>

				<div class="col-md-4 col-sm-6 gal-img">
					<a href="#gal5"><img src="images/5.jpg" alt="news image" class="img-fluid"></a>
					<a href="#gal6"><img src="images/6.jpg" alt="news image" class="img-fluid"></a>
				</div>
			</div>
		</div>
		<!-- popup-->
		<div id="gal1" class="pop-overlay animate">
			<div class="popup">
				<img src="images/1.jpg" alt="Popup Image" class="img-fluid" />
				<p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->

		<!-- popup-->
		<div id="gal2" class="pop-overlay animate">
			<div class="popup">
				<img src="images/2.jpg" alt="Popup Image" class="img-fluid" />
				<p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
		<!-- popup-->
		<div id="gal3" class="pop-overlay animate">
			<div class="popup">
				<img src="images/3.jpg" alt="Popup Image" class="img-fluid" />
				<p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup3 -->
		<!-- popup-->
		<div id="gal4" class="pop-overlay animate">
			<div class="popup">
				<img src="images/4.jpg" alt="Popup Image" class="img-fluid" />
				<p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
		<!-- popup-->
		<div id="gal5" class="pop-overlay animate">
			<div class="popup">
				<img src="images/5.jpg" alt="Popup Image" class="img-fluid" />
				<p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
		<!-- popup-->
		<div id="gal6" class="pop-overlay animate">
			<div class="popup">
				<img src="images/6.jpg" alt="Popup Image" class="img-fluid" />
				<p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
	</section>
	<!--// Galeria  -->

	<!-- stats-->
	<section class="stats-info" id="facts">
		<div class="overlay py-5">
			<div class="container py-lg-5">
				<p class="center text-center">Our Fashion Models</p>
				<h3 class="heading mb-5 text-center">Facts About Us</h3>
				<div class="row">
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-female"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">1500+</p>
								<p class="text-li">Fashion Models</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile mt-sm-0 mt-4">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-trophy"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">800</p>
								<p class="text-li">Awards Won</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile mt-lg-0 mt-4">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-weibo"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">600</p>
								<p class="text-li">Fashion Shows</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile mt-lg-0 mt-4">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-smile-o"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">1000+</p>
								<p class="text-li">Positive Feedback</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //stats -->

	<!-- contacto -->
	<section class="contact py-5" id="contact">
		<div class="container py-md-5">
			<div class="row">
				<div class="col-lg-4 contact-left">
					<p>Contacta con nosotros </p>
					<h3 class="heading">Get In Touch</h3>
					<p class="mt-3">If you have any queries, or want to speak with us, Please contact us for more details by below
						given information.</p>

				</div>
				<div class="col-lg-8 about-text">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25471195.108102433!2d-21.676556475283963!3d38.80321169797352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc42e3783261bc8b%3A0xa6ec2c940768a3ec!2zRXNwYcOxYQ!5e0!3m2!1ses!2ses!4v1586875129198!5m2!1ses!2ses" 
						class="map" style="border:0" allowfullscreen=""></iframe>
				</div>
				<div class="col-lg-8 mt-5">
					<form action="#" method="post">
						<div class="row main-w3layouts-sectns">
							<div class="col-md-6 w3-btm-spc form-text1">
								<input type="text" name="Name" placeholder="Enter Your Name" required="">
							</div>
							<div class="col-md-6 w3-btm-spc form-text2">
								<input type="text" name="Phone no" placeholder="Enter Phone Number" required="">
							</div>
						</div>
						<div class="row main-w3layouts-sectns">
							<div class="col-md-6 w3-btm-spc form-text1">
								<input type="email" name="email" placeholder="Escribe tu email" required="">
							</div>
							<div class="col-md-6 w3-btm-spc form-text2">
								<input type="text" name="subject" placeholder="Subject" required="">
							</div>
						</div>
						<div class="main-w3layouts-sectns ">
							<div class="w3-btm-spc form-text2 p-0">
								<textarea placeholder="Pon tu comentario aqui"></textarea>
							</div>
						</div>
						<button class="btn">Enviar</button>
					</form>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="contact-info">
						<div class="footer-style-w3ls">
							<p><span class="fa fa-map-marker" aria-hidden="true"></span><strong> Localización</strong> : ,
								Portal nº2, Piso Nº2A, España.</p>
						</div>
						<div class="footer-style-w3ls mt-2">
							<p><span class="fa fa-phone" aria-hidden="true"></span><strong> Telefono</strong> : +942 67 14 29</p>
						</div>
						<div class="footer-style-w3ls mt-2">
							<p><span class="fa fa-fax" aria-hidden="true"></span><strong> Fax</strong> : +121 098 8807 9987</p>
						</div>
						<div class="footer-style-w3ls mt-2">
							<p><span class="fa fa-envelope" aria-hidden="true"></span><strong> Email</strong> : <a
									href="mailto:info@example.com">DOOMWIKIAPPASSIST@gmail.com</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //contacto -->

	<!-- Newsletter -->
	<section class="subscribe-text py-5" id="subscribe">
		<div class="container py-lg-3">
			<div class="row">
				<div class="col-lg-6">
					<h3 class="heading mb-3">Subscribe newsletter</h3>
					<p>By subscribing to our mailing list you will always get latest Fashion news and updates from us. Please do
						subscribe!</p>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<form action="#" method="post">
						<input type="email" name="Email" placeholder="Pon tu email..." required="">
						<button class="btn1"><span class="fa fa-paper-plane" aria-hidden="true"></span></button>
						<div class="clearfix"> </div>
					</form>
					<p class="mt-3">We respect your privacy and will never share your email address with any person or
						organization.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- //Newsletter -->

	<!-- PIE DE PAGINA -->
	<footer class="footerv4-w3ls py-sm-5 py-4" id="footer">
		<div class="footerv4-top">
			<div class="container">

				<!-- footer logo -->
				<div class="footer-logo text-center">
					<a href="index.html"><span class="fa fa-stumbleupon" aria-hidden="true"></span> Fashion Trendz </a>
				</div>
				<!-- //PIE DE PAGINA -->

				<!-- footer nav links -->
				<div class="d-flex align-items-center footer-nav-wthree justify-content-center my-3">
					<ul class="footer-link">
						<li>
							<a href="index.html">Inicio</a>
						</li>
						<li>
							<a href="#about">Sobre nosotros</a>
						</li>
						<li>
							<a href="#services">Servicios</a>
						</li>
						<li>
							<a href="#gallery">Galeria</a>
						</li>
						
					</ul>
				</div>
				<!-- //footer nav links -->

				<!-- move top icon -->
				<div class="text-center">
					<a href="#home" class="move-top text-center"><span class="fa fa-angle-double-up"
							aria-hidden="true"></span></a>
				</div>
				<!-- //move top icon -->

				<!-- copyright -->
				<div class="copy-right text-center">
					<p>© 2019 Fashion Trendz. All rights reserved | Design by
						<a href="http://w3layouts.com"> W3layouts.</a>
					</p>
				</div>
				<!-- //copyright -->


			</div>
		</div>
	</footer>
	<!-- //footer -->


	<!-- main slider -->
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script>
		$(document).ready(function () {
			$('.owl-one').owlCarousel({
				loop: true,
				margin: 0,
				nav: false,
				dots: true,
				responsiveClass: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 1000,
				autoplayHoverPause: false,
				responsive: {
					0: {
						items: 1,
						nav: false
					},
					480: {
						items: 1,
						nav: false
					},
					667: {
						items: 1,
						nav: false
					},
					1000: {
						items: 1,
						nav: false
					}
				}
			})
		})
	</script>
	<!-- /main slider -->

</body>

</html>