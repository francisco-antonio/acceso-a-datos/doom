<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Razas */

$this->title = 'Update Razas: ' . $model->nombre_raza;
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre_raza, 'url' => ['view', 'id' => $model->nombre_raza]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="razas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
