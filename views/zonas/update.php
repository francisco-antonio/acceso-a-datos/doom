<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Zonas */

$this->title = 'Update Zonas: ' . $model->cod_zona;
$this->params['breadcrumbs'][] = ['label' => 'Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_zona, 'url' => ['view', 'id' => $model->cod_zona]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="zonas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
