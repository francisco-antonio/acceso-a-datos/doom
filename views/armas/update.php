<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Armas */

$this->title = 'Update Armas: ' . $model->numero;
$this->params['breadcrumbs'][] = ['label' => 'Armas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numero, 'url' => ['view', 'id' => $model->numero]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="armas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
