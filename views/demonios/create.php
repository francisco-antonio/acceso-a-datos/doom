<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Demonios */

$this->title = 'Create Demonios';
$this->params['breadcrumbs'][] = ['label' => 'Demonios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="demonios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
