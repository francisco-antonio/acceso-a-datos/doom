<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Demonios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="demonios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Demonios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_demonio',
            'cod_personaje',
            'numero',
            'nombre_razas',
            'nombre_mapas',
            //'nombre_demonio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
