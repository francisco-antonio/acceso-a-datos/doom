<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mapas */

$this->title = 'Update Mapas: ' . $model->nombre_mapa;
$this->params['breadcrumbs'][] = ['label' => 'Mapas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre_mapa, 'url' => ['view', 'id' => $model->nombre_mapa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mapas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
